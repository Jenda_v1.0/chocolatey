# Chocolatey package configurations

Custom package settings for quick initial setup of applications on Windows using [Chocolatey](https://chocolatey.org/).

## Installation

1. Install Chocolatey, see [installation guide](https://docs.chocolatey.org/en-us/choco/setup#installing-chocolatey).
1. Download a _[.config](https://docs.chocolatey.org/en-us/choco/commands/install#packages.config)_ file of your choice
   or create one.
    1. _Packages can be found [here](https://community.chocolatey.org/packages)._
1. Run the following command as administrator `choco install <path-to-my-package.config> -y`.
    1. _`-y` parameter confirm all prompts. Therefore, affirmative answer will be chosen instead of a prompting._
